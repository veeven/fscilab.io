---
title: "Statement on Arogya Setu Source Code Release under Apache License"
date: 2020-06-01T03:47:36+05:30
author: "false"
tags: ["arogyasethu"]
discussionlink: https://codema.in/d/vhoA0lCM/aarogya-setu-app-is-released-as-free-software-we-should-respond-to-this-with-a-statement
draft: false
---
## Background:

In a country where there is a stated policy on adoption of Free Software (sometimes called as open source), it is rather surprising that a public application supported, promoted, and partially mandated by the government is not Free Software from the very inception. While the server side source code still remains undisclosed and several unresolved technical questions exist including about reproducibility of builds (to verify the application distributed via Google's play store is really built from the source code published) and whether penetration testing is acceptable, we use the attention brought on transparency to raise larger issues about Aarogya Setu application and government's use of technology in handling the most dreadful pandemic of our times.

The respected CEO of NITI Aayog, Amitabh Kant, during the press conference on 26th May stated that "transparency, privacy, and security" were the core design principles of Aarogya Setu. As a group of people who build software in various domains that respect these very same principles among others, we find Aarogya Setu lacking in all these three principles, despite the claims by the government on the contrary.

## Privacy:

The application is described as "privacy-first by design". Without a legal framework for personal data protection like GDPR in Europe, there is no mechanism through which a citizen can upload data to a centralized server and be assured that the data will be handled as explained by the developers. Would there be a legal recourse available to them if it turns out that the data was eventually handled in a different way than what was explained when being uploaded, for example being made accessible to third parties? We find it problematic that everyone's data is connected to a centralized server run by the government, thus putting every citizen vulnerable to government surveillance. We are forced to question whether such large scale surveillance is justified considering the narrow utility of the application.  

## Transparency:

On the matter of transparency, there are uncertainties regarding the origin, design, running, and continued updation of the platform. It is widely known that various private companies are involved in the development of the platform. Complete transparency would entail disclosure of the extent of such involvement, the processes followed in such public-private collaboration, inlcluding disclosure of tenders or contracts given to private companies for the work they contributed in the app, the guarantees available to the public about strict separation of data from the hands of private collaborators, and also details on procedures which allow more stakeholders, including civil society and rights activists, to shape the further development of the platform.

## Privacy vs Released Source Code:
Public forums and mainstream media seems to think releasing source code alone brings transparency, rather it gives a very *false sense of transparency*. Releasing source code alone doesn't guarantee transparency, especially when server-side is involved.

* What is the guarantee that the server is running the released source code (if available)?
* What is the guarantee that the raw data is not processed by undisclosed tools?

These questions can be answered only when we don't have to blindly trust the govt and instead the claims can be independantly verified by third parties by running the source code on independent servers (decentralized/federated design instead of centralized server in case of Aarogya Setu). Since it is a centralized service by design, we also call for independent security and source audit of application running on the government servers to be sure.

## Security:

In the security world, no organization, no matter how advanced they are, usually proclaims their product or platform to be "secure" while it still hasn't withstood the test of the time. It is understandable that the government would want to give public confidence in using the application, but we feel responsible to point out that it is reasonable to assume that an application hastily built during a pandemic is ripe with security blunders that are waiting to be discovered. Only time will tell how many security vulnerabilities get discovered on the platform and how much damage such vulnerabilities would cause on our citizens.

On an entirely different level, the application is already giving a dangerously large number of citizens a false sense of "security" whereby they feel safe and go about misinterpreting the green indicator given by the application. It is necessary that the pandemic which is a global public health crisis be treated as such and that the response to that be led by time-tested public health measures rather than untested, and unproven technologies.

## Next Step:

We are evaluating alternate apps and protocols used across the world right now and will come up with a follow up statement later with our recommendations.


*See the original discussion thread on [codema.in](https://codema.in/d/vhoA0lCM/aarogya-setu-app-is-released-as-free-software-we-should-respond-to-this-with-a-statement) for more details.*

