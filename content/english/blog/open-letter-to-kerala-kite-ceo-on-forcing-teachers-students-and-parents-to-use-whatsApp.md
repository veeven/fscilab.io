---
title: "Open Letter to KITE CEO on Forcing Teachers Students and Parents to Use WhatsApp"
date: 2020-06-17T14:48:17+05:30
author: "false"
tags: ["whatsapp", "kite", "kerala", "statement"]
discussionlink: https://codema.in/d/LDwsanKx/open-letter-to-kite-ceo-on-forcing-teachers-students-and-parents-to-use-whatsapp
draft: false
---
### Introduction

This is an open letter to Kerala Infrastructure and Technology for Education (KITE - https://kite.kerala.gov.in/) CEO from Free Software Community of India (FSCI - https://fsci.in). We are writing to bring into your attention some issues regarding the ongoing online learning programme 'First Bell' and some possible solutions. Since our group focus on Free Software related issues, we are limiting our comment to Free Software aspect of the programme only.

### Background
First of all, we really appreciate the effort the former IT@School and KITE has taken to liberate the school community of Kerala by presenting, developing and popularising Free Software (free as in freedom).

But many school communities are now forcing WhatsApp on people who do not want to use it <sup>[1]</sup>, and the whole move to Free Software is being hijacked by the overuse and mandating of WhatsApp in school communities. Facebook <sup>[2]</sup> (the parent company of Whatsapp) has a bad reputation for privacy, especially after the Cambridge Analytica issue <sup>[3]</sup>. KITE should be promoting better alternatives like Moodle Learning Management System and Matrix based messaging services <sup>[4]</sup>, which are powered by Free Software and do not force anyone to depend on a single foreign company with a bad reputation for privacy.

### In detail
Many are reluctant to use software like WhatsApp due to concerns about privacy. It would be unethical to force them to use it or act as agents of these software.​​​​​​​ Also, in case of WhatsApp, people will not be even able to make use of a computer, for eg. in a public library, without a smartphone. Many parents and teachers are being mentally tortured to buy a smart phone and install WhatsApp, which, some managed to resist <sup>[5]</sup>. Of course the school community should embrace advancements, but only if they aren't harmful. Better, they should promote libre alternatives and provide options so that everybody can participate.

The "shift to online" for education is now a wasteland of hasty choices and convenience around short term goals overcoming deployment of durable and correct systems. Aside from WhatsApp for a medium of exchange of content - homework, assignments, etc - there are plentiful anecdotes of schools asking parents to open up a specific pattern based email accounts (preferably GMail) which is proposed as an alternative if smartphones and such are unavailable. Ad-hoc decisions in school committees often driven by the pressure to be seen as doing something that is modern is a good enough way to start with the part of bad decisions.

We have learned that, directions from KITE and Education department was about using 'messaging applications', without mentioning any specific services <sup>[6][7]</sup>, but DIET Kozhikode guidelines specifically mandates WhatsApp use <sup>[1]</sup>. The issue of scaling (handling large number of users simultaneously using the service) self-hosted Learning Management Systems like Moodle was also raised as one issue stopping adoption of such an option.

There are many apps using Matrix protocol <sup>[8]</sup> giving a choice of application for users on mobile or desktop/laptop. Riot is the most feature complete application for Matrix and there are applications like Fluffy Chat which provides a simpler interface. It also provides an option to use it from a any web browser, making it possible to use from any public computer, like a computer in a public library.

### Recommendations

1. We feel KITE should introduce other communication options, which do not lock users to a specific propreitary service should be promoted among teachers. Students and parents should be given an option to choose a federated instant messaging service or email as an alternative to forcing WhatsApp. Our community is willing to provide such services or suggest service providers and train teachers interested to use these options. More than 25 people from the Free Software community have already volunteered to support the teachers <sup>[9]</sup>.

2. We can also offer to setup and manage at least one Moodle instance if some schools wants to opt in for choosing Moodle. We could also talk to Engineering colleges to check the possibility of more students stepping up to volunteer more such Moodle instances.

3. Currently the videos are uploaded on Youtube and Facebook after the show on Victers channel is over. KITE should also consider offering PeerTube as another option, which is Free Software powered and offers better privacy to users. We have our own instance of PeerTube at https://videos.fsci.in and we can offer to mirror the videos via this platform. We can offer to create a channel for Victers and you can upload videos to this instance or we can volunteer to pull the videos from Youtube directly if you choose a Creative Commons license which allows sharing freely.

### Contacting us
Any teachers interested in offering these Free Software based services without lock-in to a specific company may use one of these options to reach us.

1. Post their request on our online discussion group at https://codema.in/free-software-community-of-india-fsci/

2. Contact us via email - admin at fsci.in

3. Contact via phone or sms - Praveen (+91 9561745712) or Tanzeem (+91 9446705956, +91 9746495247)


### References

[1]:(https://www.doolnews.com/online-learning-parents-and-teachers-need-attention-education-department-guidelines-454.html)

[2]:(https://www.fsf.org/facebook)

[3]:(https://www.reuters.com/article/us-facebook-cambridge-analytica-factbox/factbox-who-is-cambridge-analytica-and-what-did-it-do-idUSKBN1GW07F)

[4]:(https://medium.com/we-distribute/riot-a-decentralized-slack-like-messenger-powered-by-matrix-25f9b72cd24)

[5]:(http://lists.smc.org.in/pipermail/discuss-smc.org.in/2020-June/003627.html)

[6]:(http://lists.smc.org.in/pipermail/discuss-smc.org.in/2020-June/003643.html)

[7]:(https://education.kerala.gov.in/wp-content/uploads/2020/05/GO-Online-Class-First-Bell.pdf)

[8]:(https://yewtu.be/watch?v=jr2mXSKq3B4)

[9]:(https://codema.in/d/LDwsanKx/open-letter-to-kerala-it-school-kite-director-on-forcing-teachers-students-and-parents-to-use-whatsapp/5)

1. https://www.doolnews.com/online-learning-parents-and-teachers-need-attention-education-department-guidelines-454.html (we have a copy of the document referenced in this article)

2. https://www.fsf.org/facebook

3. https://www.reuters.com/article/us-facebook-cambridge-analytica-factbox/factbox-who-is-cambridge-analytica-and-what-did-it-do-idUSKBN1GW07F

4. https://medium.com/we-distribute/riot-a-decentralized-slack-like-messenger-powered-by-matrix-25f9b72cd24

5. http://lists.smc.org.in/pipermail/discuss-smc.org.in/2020-June/003627.html

6. http://lists.smc.org.in/pipermail/discuss-smc.org.in/2020-June/003643.html

7. https://education.kerala.gov.in/wp-content/uploads/2020/05/GO-Online-Class-First-Bell.pdf

8. https://yewtu.be/watch?v=jr2mXSKq3B4

9. https://codema.in/d/LDwsanKx/open-letter-to-kerala-it-school-kite-director-on-forcing-teachers-students-and-parents-to-use-whatsapp/5

<br>

*See the original discussion on [codema.in](https://codema.in/d/LDwsanKx/open-letter-to-kerala-it-school-kite-director-on-forcing-teachers-students-and-parents-to-use-whatsapp/54) for discussions related to this statement.*
